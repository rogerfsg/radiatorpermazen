package com.paulhammant.buildradiator.radiator.store;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.paulhammant.buildradiator.radiator.TestRandomGenerator;
import com.paulhammant.buildradiator.radiator.model.Radiator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class BackedByPermazenDataStoreTest {

    private String radCode;
    private Radiator rad;
    private int called;
    private RadiatorStore radStore;

    @Before
    public void setup() {
        radCode = null;
        rad = null;
        called = 0;
        radStore = null;
    }

    @After
    public void tearDown() {
        if (radStore != null) {
            radStore.stopSaver();
        }
    }

    @Test
    public void saveDataAndRecovery() throws JsonProcessingException {

        BackedByPermazenDataStore x = new BackedByPermazenDataStore();
        x.saveInDataService(
                "QWERTY",
                x.createRadiator(new TestRandomGenerator("QWERTY", "sseeccrreett"), "A"));


        Radiator rad = x.getFromDataService("QWERTY");
        assertThat(rad.code, equalTo("QWERTY"));
        assertThat(rad.codeAndSecretOnly().code, equalTo("QWERTY"));
        assertThat(rad.codeAndSecretOnly().secret, equalTo("sseeccrreett"));
    }

    @Test
    public void validateIfTwoSavesForSameRadiatorCodeImplyNotAnEror(){
        BackedByPermazenDataStore x = new BackedByPermazenDataStore();
        x.saveInDataService(
                "QWERTY",
                x.createRadiator(new TestRandomGenerator("QWERTY", "sseeccrreett"), "A"));

        x.saveInDataService(
                "QWERTY",
                x.createRadiator(new TestRandomGenerator("QWERTY", "sseeccrreett"), "B"));

        Radiator rad = x.getFromDataService("QWERTY");
        assertThat(rad.code, equalTo("QWERTY"));
        assertThat(rad.codeAndSecretOnly().code, equalTo("QWERTY"));
        assertThat(rad.stepNames[0], equalTo("B"));
    }
}
