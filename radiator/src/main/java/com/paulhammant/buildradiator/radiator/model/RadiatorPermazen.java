package com.paulhammant.buildradiator.radiator.model;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.istack.internal.NotNull;
import io.permazen.JObject;
import io.permazen.JTransaction;
import io.permazen.annotation.JField;
import io.permazen.annotation.PermazenType;
import io.permazen.core.ObjId;
import io.permazen.tuple.Tuple2;

import java.io.IOException;
import java.util.NavigableSet;
import java.util.Optional;

@PermazenType
public abstract class RadiatorPermazen implements io.permazen.JObject {

    @NotNull
    @JField(indexed = true, unique = true)
    public abstract  String getCode();
    public abstract void setCode(String code);


    public abstract String getJsonRadiator();
    public abstract void setJsonRadiator(String jsonRadiator);

    @Override
    public String toString(){
        return getCode();
    }
    public static NavigableSet<RadiatorPermazen> getAll() {
        return JTransaction.getCurrent().getAll(RadiatorPermazen.class);
    }


    @Override
    public ObjId getObjId() {
        return new ObjId(getCode());
    }
    public static RadiatorPermazen create() {
        return JTransaction.getCurrent().create(RadiatorPermazen.class);
    }

    public Radiator factoryRadiator() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        TypeReference<Radiator> typeRef
                = new TypeReference<Radiator>() {};

        Radiator r = objectMapper.readValue(this.getJsonRadiator(), typeRef);
        return r;
    }

    // Find user by username
    public static RadiatorPermazen getByCode(String code) {
        Optional<RadiatorPermazen> rp = RadiatorPermazen.getAll().stream()
                .filter(p -> p.getCode() != null && p.getCode().equals(code))
                .findAny();
        //If the radiator with the code does not exists
        if(! rp.isPresent())
            return null;
        return rp.get();
    }


}
