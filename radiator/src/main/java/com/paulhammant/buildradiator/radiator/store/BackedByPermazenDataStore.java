package com.paulhammant.buildradiator.radiator.store;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.paulhammant.buildradiator.radiator.model.Radiator;
import com.paulhammant.buildradiator.radiator.model.RadiatorPermazen;
import io.permazen.JTransaction;
import org.jooby.mvc.Local;

import java.io.File;
import java.util.Optional;

public class BackedByPermazenDataStore  extends RadiatorStore  {

    PermazenRepository repository;
    public BackedByPermazenDataStore() {
        super(true);

        this.repository=new PermazenRepository ();
    }

    @Override
    protected void saveInDataService(String radCode, Radiator rad) {
        repository.open();
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            final String jsonRad;

            jsonRad = objectMapper.writeValueAsString(rad);

            repository.doInTransaction(() -> {
                RadiatorPermazen rp= RadiatorPermazen.getByCode(radCode);
                if(rp == null) {
                    rp = RadiatorPermazen.create();
                    rp.setCode( radCode);
                }
                rp.setJsonRadiator (jsonRad);
            });
        } catch (JsonProcessingException e) {
            //TODO user will not know about the exception, change that
            System.err.println("Error while doing db operation");
            System.err.println(e.getMessage());
            e.printStackTrace();
        }finally{

            repository.close();
        }
    }


    @Override
    protected Radiator getFromDataService(final String radCode) {
        repository.open();
        final JTransaction jtx = repository.factoryJTransaction();

        try {
           RadiatorPermazen rp = RadiatorPermazen.getByCode(radCode);
            if(rp != null){
                Radiator r =  rp.factoryRadiator();
                jtx.commit();
                return r;
            } else return null;



        } catch (Exception e) {
            jtx.rollback();
            //TODO or remove definitelly the radiator from repository once json is not well formatted
            System.err.println("Error while doing json db operation");
            System.err.println(e.getMessage());
            e.printStackTrace();
            return null;
        } finally {
            repository.close();
        }
    }
}

