package com.paulhammant.buildradiator.radiator.store;

import com.google.inject.Binder;
import com.typesafe.config.Config;
import org.jooby.Env;
import org.jooby.Jooby;

public class StoreService implements Jooby.Module{

    private RadiatorStore rs;

    @Override
    public void configure(Env env, Config conf, Binder binder) throws Throwable {

        if(conf != null && conf.hasPath("Radiator.Storage")){

            switch (conf.getString("Radiator.Storage")){

                case "permazen":
                    rs = new BackedByPermazenDataStore();
                    break;
                case "google":
                    rs = new BackedByGoogleCloudDataStore(conf.getString("Google.ProjectId"));
                    break;
                default:
                    rs = new RadiatorStore();
                    break;
            }
        }else rs = new RadiatorStore();
    }

    public RadiatorStore getRadiatorStore() {
        return rs;
    }
}
