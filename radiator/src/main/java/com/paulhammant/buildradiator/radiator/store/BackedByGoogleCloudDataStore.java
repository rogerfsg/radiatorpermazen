package com.paulhammant.buildradiator.radiator.store;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.datastore.v1.*;
import com.google.datastore.v1.client.Datastore;
import com.google.datastore.v1.client.DatastoreException;
import com.google.datastore.v1.client.DatastoreFactory;
import com.google.datastore.v1.client.DatastoreHelper;
import com.google.protobuf.ByteString;
import com.paulhammant.buildradiator.radiator.model.Radiator;
import com.typesafe.config.Config;

import java.io.IOException;
import java.security.GeneralSecurityException;

public class BackedByGoogleCloudDataStore extends RadiatorStore {

    private Datastore datastore;
    private ObjectMapper om = new ObjectMapper();

    public BackedByGoogleCloudDataStore(String projectId) {
        super(true);

        // Set the project ID from the command line parameters.
//        String projectId = System.getenv("Google.ProjectId");
//        Config conf = require(Config.class);
//        conf.getString("Google.ProjectId")

        // Setup the connection to Google Cloud Datastore and infer credentials
        // from the environment.
        try {
            datastore = DatastoreFactory.get().create(DatastoreHelper.getOptionsFromEnv()
                    .projectId(projectId).build());
        } catch (GeneralSecurityException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected Radiator getFromDataService(String radCode) {
        try {
            // Create an RPC request to begin a new transaction.
            BeginTransactionRequest.Builder treq = BeginTransactionRequest.newBuilder();
            // Execute the RPC synchronously.
            BeginTransactionResponse tres = datastore.beginTransaction(treq.build());
            // Get the transaction handle from the response.
            ByteString tx = tres.getTransaction();

            // Create an RPC request to get entities by key.
            LookupRequest.Builder lreq = LookupRequest.newBuilder();
            // Set the entity key with only one `path`: no parent.
            Key.Builder key = Key.newBuilder().addPath(
                    Key.PathElement.newBuilder()
                            .setKind("Radiator")
                            .setName(radCode));
            // Add one key to the lookup request.
            lreq.addKeys(key);
            // Set the transaction, so we get a consistent snapshot of the
            // entity at the time the transaction started.
            lreq.getReadOptionsBuilder().setTransaction(tx);
            // Execute the RPC and get the response.
            LookupResponse lresp = datastore.lookup(lreq.build());
            // Create an RPC request to commit the transaction.
            CommitRequest.Builder creq = CommitRequest.newBuilder();
            // Set the transaction to commit.
            creq.setTransaction(tx);
            Entity entity;
            if (lresp.getFoundCount() > 0) {
                entity = lresp.getFound(0).getEntity();
            } else {
                return null;
            }

            return om.readValue((String) entity.getProperties().get("rad").getStringValue(), Radiator.class);

        } catch (DatastoreException e) {
            // Catch all Datastore rpc errors.
            System.err.println("Error while doing datastore operation");
            // Log the exception, the name of the method called and the error code.
            System.err.println(String.format("DatastoreException(%s): %s %s",
                    e.getMessage(),
                    e.getMethodName(),
                    e.getCode()));
        } catch (IOException e) {
            System.err.println("Error while doing objectMapper operation");
            System.err.println(e.getMessage());
        }

        return null;
    }

    @Override
    protected void saveInDataService(String radCode, Radiator rad) {

        try {
            BeginTransactionRequest.Builder treq = BeginTransactionRequest.newBuilder();

            BeginTransactionResponse tres = null;
            tres = datastore.beginTransaction(treq.build());
            // Get the transaction handle from the response.
            ByteString tx = tres.getTransaction();

            CommitRequest.Builder creq = CommitRequest.newBuilder();

            creq.setTransaction(tx);

            Entity entity;

            Key.Builder key = Key.newBuilder().addPath(
                    Key.PathElement.newBuilder()
                            .setKind("Radiator")
                            .setName(radCode));

            LookupRequest.Builder lreq = LookupRequest.newBuilder();
            lreq.addKeys(key);
            // Set the transaction, so we get a consistent snapshot of the
            // entity at the time the transaction started.
            lreq.getReadOptionsBuilder().setTransaction(tx);
            LookupResponse lresp = datastore.lookup(lreq.build());

            // If no entity was found, create a new one.
            Entity.Builder entityBuilder = Entity.newBuilder();
            // Set the entity key.
            entityBuilder.setKey(key);
            // Add two entity properties:
            // - a utf-8 string: `question`

            entityBuilder.getMutableProperties().put("rad", Value.newBuilder()
                    .setExcludeFromIndexes(true)
                    .setStringValue(om.writeValueAsString(rad)).build());
            // Build the entity.
            entity = entityBuilder.build();
            // Insert the entity in the commit request mutation.
            if (lresp.getFoundCount() == 0) {
                creq.addMutationsBuilder().setInsert(entity);
            } else {
                creq.addMutationsBuilder().setUpdate(entity);
            }

            datastore.commit(creq.build());
        } catch (DatastoreException | JsonProcessingException e) {
            System.err.println("Error while doing db operation");
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }
}

