package com.paulhammant.buildradiator.radiator.store;

import com.paulhammant.buildradiator.radiator.model.RadiatorPermazen;
import io.permazen.JTransaction;
import io.permazen.Permazen;
import io.permazen.PermazenFactory;
import io.permazen.ValidationMode;
import io.permazen.core.Database;
import io.permazen.kv.RetryTransactionException;
import io.permazen.kv.array.ArrayKVDatabase;
import io.permazen.kv.array.ArrayKVImplementation;
import io.permazen.kv.array.AtomicArrayKVStore;

import java.io.File;

public class PermazenRepository{

    private ArrayKVDatabase arrayKVDatabase;
    private Permazen permazen;

    public PermazenRepository() {
        //preparing the directory for the database
        final String dir = System.getProperty("user.dir") + "\\db\\";
        File f = new File(dir);
        if(!f.exists()) f.mkdirs();

        arrayKVDatabase = new ArrayKVImplementation().createKVDatabase(
                f,
                null,
                new AtomicArrayKVStore());
    }

    public void open(){
        // Create and start underlying database
        this.arrayKVDatabase.start();
        permazen = new PermazenFactory()
                .setDatabase(new Database(arrayKVDatabase))
                .setSchemaVersion(-1)
                .setModelClasses(RadiatorPermazen.class)
                .newPermazen();

    }

    public void close(){
        JTransaction.setCurrent(null);
        this.arrayKVDatabase.stop();
    }


    public JTransaction factoryJTransaction(){
        JTransaction jtx= permazen.createTransaction(true, ValidationMode.AUTOMATIC);
        JTransaction.setCurrent(jtx);
        return jtx;
    }



    public void doInTransaction( Runnable action) {

        int numTries = 0;
        while (true) {
            final JTransaction jtx = permazen.createTransaction(true, ValidationMode.AUTOMATIC);
            JTransaction.setCurrent(jtx);
            try {
                action.run();
                jtx.commit();
                return;
            } catch (RetryTransactionException e) {
                jtx.rollback();
                if (numTries++ == 3) {
                    throw e;
                }
                continue;
            } finally {
                JTransaction.setCurrent(null);
            }
        }
    }

}
