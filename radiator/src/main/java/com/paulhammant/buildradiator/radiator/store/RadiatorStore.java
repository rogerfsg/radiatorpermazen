package com.paulhammant.buildradiator.radiator.store;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.inject.Binder;
import com.paulhammant.buildradiator.radiator.model.Radiator;
import com.paulhammant.buildradiator.radiator.model.RadiatorDoesntExist;
import com.typesafe.config.Config;
import org.jooby.Env;
import org.jooby.Jooby;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class RadiatorStore{

    public final Map<String, Radiator> actualRadiators = new ConcurrentHashMap<>();
    private final Map<String, Long> radiatorLastSavedTimes = new ConcurrentHashMap<>();
    private boolean saverShouldKeepGoing = true;

    public RadiatorStore() {
        this(false);
    }
    protected RadiatorStore(boolean onAppEngine) {
        Thread saver = new Thread(() -> {
            while (saverShouldKeepGoing) {
                processRadiatorsToSave();
                try {
                    Thread.sleep(getMillisToDelay());
                } catch (InterruptedException e) {
                }
            }
        });
        saver.setDaemon(true);
        saver.start();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            if (onAppEngine) {
                System.out.println("Shutdown detectected .. ");
            }
            processRadiatorsToSave();
            if (onAppEngine) {
                System.out.println(".. ready for shutdown");
            }
        }));
    }

    protected int getMillisToDelay() {
        return 5 * 60 * 1000;
    }

    private void processRadiatorsToSave() {
        synchronized (radiatorLastSavedTimes) {
            Set<String> ks = actualRadiators.keySet();
            for (String radCode : ks) {
                Radiator rad = actualRadiators.get(radCode);
                Long lastSaved = radiatorLastSavedTimes.get(radCode);
                if (lastSaved == null || rad.lastUpdated > lastSaved) {
                    saveInDataService(radCode, rad);
                    radiatorLastSavedTimes.put(radCode, rad.lastUpdated);
                }
            }
        }
    }

    public Radiator get(final String radCode, String ipAddress) {
        Radiator radiator = this.actualRadiators.get(radCode);
        if (radiator == null) {
            radiator = getFromDataService(radCode);
            if (radiator != null) {
                actualRadiators.put(radCode, radiator);
                radiatorLastSavedTimes.put(radCode, radiator.lastUpdated);
            }
        }
        if (radiator != null) {
            radiator.verifyIP(ipAddress);
        } else {
            throw new RadiatorDoesntExist();
        }
        return radiator;
    }

    protected Radiator getFromDataService(String radCode) {
        return null;
    }

    protected void saveInDataService(String radCode, Radiator rad) {
        radiatorLastSavedTimes.put(radCode, rad.lastUpdated);
    }

    public Radiator createRadiator(RandomGenerator codeGenerator, String... steps) {
        String radiatorCode = codeGenerator.generateRadiatorCode();
        String secret = codeGenerator.generateSecret();
        Radiator radiator = new Radiator(radiatorCode, secret, steps);
        this.actualRadiators.put(radiatorCode, radiator);
        return radiator;
    }

    public void stopSaver() {
        saverShouldKeepGoing = false;
    }

}
